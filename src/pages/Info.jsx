import React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'
import InfoUser from '../components/InfoUser'

function Info() {
    return (
        <div>
            <Header />
            <InfoUser />
            <Footer />
        </div>
    )
}

export default Info;

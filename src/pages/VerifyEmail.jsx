import React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'
import VerifyEmailComponent from '../components/VerifyEmail'

function VerifyEmail() {
    return (
        <div>
            <Header />
            <VerifyEmailComponent />
            <Footer />
        </div>
    )
}

export default VerifyEmail;

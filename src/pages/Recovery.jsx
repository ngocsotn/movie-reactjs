import React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'
import RecoveryCom from '../components/Recovery'

function RecoveryPassword() {
    return (
        <div>
            <Header />
            <RecoveryCom />
            <Footer />
        </div>
    )
}

export default RecoveryPassword

import React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'
import ForgotPasswordCom from '../components/ForgotPassword'

function ForgotPassword() {
    return (
        <div>
            <Header />
            <ForgotPasswordCom />
            <Footer />
        </div>
    )
}

export default ForgotPassword

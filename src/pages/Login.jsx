import React from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import LoginComponent from "../components/Login";

function Login() {
    return (
        <div>
            <Header />
            <LoginComponent />
            <Footer />
        </div>
    );
}

export default Login;

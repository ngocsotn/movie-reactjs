import React from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import SearchComponent from "../components/Search";
function Search() {
    return (
        <div>
            <Header />
            <SearchComponent />
            <Footer />
        </div>
    );
}

export default Search;
